#![recursion_limit = "256"]
extern crate proc_macro;

use crate::proc_macro::TokenStream;
use quote::quote;
use syn::{Data, DeriveInput, Fields};

#[proc_macro_attribute]
pub fn model(attr: TokenStream, input: TokenStream) -> TokenStream {
    let mut ast = syn::parse(input).expect("Cannot parse input");

    let table_name = attr.to_string();

    impl_model_macro(table_name, &mut ast)
}

fn impl_model_macro(table_name: String, ast: &mut DeriveInput) -> TokenStream {
    let name = ast.ident.clone();
    // let name2 = ast.ident.clone();

    let data = &mut ast.data;

    let res = match data {
        Data::Struct(data) => match &mut data.fields {
            Fields::Named(fields) => {
                let named = &fields.named;

                let lower_name = name.to_string().to_ascii_lowercase();

                let lower_name_ident: syn::Ident = syn::parse_str(&lower_name).unwrap();

                // tricks to have a one-line iterator to create "impl #new_name_ident {}"
                let mut lower_name_ident_vec = vec![];

                for _ in &fields.named {
                    lower_name_ident_vec.push(lower_name_ident.clone());
                }

                // tricks to have a one-line iterator to create "pub fn fetch_by_ {}"
                let mut name_vec = vec![];

                for _ in &fields.named {
                    name_vec.push(name.clone());
                }

                let name_vec2 = name_vec.clone();

                let new_name = "New".to_string() + &name.to_string();
                ast.ident = syn::parse_str(&new_name).unwrap();

                let new_name_ident = ast.ident.clone();

                let table_name = if table_name.len() == 0 {
                    lower_name.clone() + &"s"
                } else {
                    table_name.replace("\"", "")
                };

                let table_name_ident: syn::Ident = syn::parse_str(&table_name).unwrap();
                // let table_name_ident2 = table_name_ident.clone();

                // tricks to have a one-line iterator to create "pub fn fetch_by_ {}"
                let mut table_name_ident_vec = vec![];

                for _ in &fields.named {
                    table_name_ident_vec.push(table_name_ident.clone());
                }

                let table_name_ident_vec2 = table_name_ident_vec.clone();

                let from_name: syn::Ident =
                    syn::parse_str(&("from_".to_string() + &lower_name)).unwrap();

                let mut fields_idents = vec![];

                for field in &fields.named {
                    fields_idents.push(field.ident.clone().unwrap());
                }

                let fields_idents2 = fields_idents.clone();
                let fetch_fields_idents3 = fields_idents
                    .clone()
                    .iter()
                    .map(|x| syn::parse_str(&("fetch_by_".to_string() + &x.to_string())).unwrap())
                    .collect::<Vec<syn::Ident>>();

                let fields_idents4 = fields_idents2.clone();

                quote!(
                    #[derive(Identifiable, Serialize, Deserialize, Queryable, Debug, Clone, Associations, AsChangeset)]
                    #[table_name = #table_name]
                    #[changeset_options(treat_none_as_null = "true")]
                    pub struct #name {
                        pub id: i32,
                        #named
                    }

                    // #[changeset_options(treat_none_as_null = "true")].
                    #[derive(AsChangeset, Insertable, Debug, Clone, Serialize, Deserialize)]
                    #[table_name = #table_name]
                    #[changeset_options(treat_none_as_null = "true")]
                    #ast

                    impl #new_name_ident {
                        pub fn #from_name(#lower_name_ident: &#name) -> #new_name_ident {
                            #new_name_ident {
                                #(#fields_idents: #lower_name_ident_vec.#fields_idents2.clone()),*
                            }
                        }
                    }

                    impl #name {
                        pub fn create(#lower_name_ident: #name, conn: &diesel::PgConnection) -> std::result::Result<#name, diesel::result::Error> {
                            let #lower_name_ident = #new_name_ident::#from_name(&#lower_name_ident);

                            diesel::insert_into(#table_name_ident::table)
                                .values(&#lower_name_ident)
                                .execute(conn)?;

                            #table_name_ident::table
                                .order(#table_name_ident::id.desc())
                                .first(conn)
                        }

                        pub fn list(conn: &diesel::PgConnection) -> std::result::Result<Vec<#name>, diesel::result::Error> {
                            #table_name_ident::table
                                .order(#table_name_ident::id)
                                .load::<#name>(conn)
                        }

                        pub fn update(id: i32, #lower_name_ident: #name, conn: &diesel::PgConnection) -> std::result::Result<usize, diesel::result::Error> {
                            let #lower_name_ident = #new_name_ident::#from_name(&#lower_name_ident);

                            diesel::update(#table_name_ident::table.find(id))
                                .set(&#lower_name_ident)
                                .execute(conn)
                        }

                        pub fn save(&mut self, conn: &diesel::PgConnection) -> std::result::Result<(), diesel::result::Error> {
                            if self.id == -1 {
                                let res = #name::create(self.clone(), conn)?;

                                self.id = res.id;

                            } else {
                                #name::update(self.id, self.clone(), conn)?;
                            }

                            Ok(())
                        }

                        pub fn delete(id: i32, conn: &diesel::PgConnection) -> std::result::Result<usize, diesel::result::Error> {
                            diesel::delete(#table_name_ident::table.find(id))
                                .execute(conn)
                        }

                        pub fn fetch(id_given: i32, conn: &diesel::PgConnection) -> std::result::Result<#name, diesel::result::Error> {
                            use schema::#table_name_ident::dsl::*;

                            #table_name_ident.filter(id.eq(id_given)).first::<#name>(conn)
                        }

                        // #(
                        //     pub fn #fetch_fields_idents3(value_given: i64, conn: &diesel::PgConnection) -> Result<#name_vec, diesel::result::Error> {
                        //         use schema::#table_name_ident_vec::dsl::*;

                        //         #table_name_ident_vec2.filter(#fields_idents4.eq(value_given)).first::<#name_vec2>(conn)
                        //     }
                        // )*

                    }
                )
            }
            _ => panic!("Orm 'model': The target must be a named struct."),
        },
        _ => panic!("Orm 'model': The target must be a named struct."),
    };

    res.into()
}
