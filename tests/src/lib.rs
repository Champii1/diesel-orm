#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate orm_macro_derive;

mod schema;

#[cfg(test)]
mod tests {
    use super::*;
    use diesel::prelude::*;
    use schema::tests;

    const DATABASE_URL: &str = "postgres://dbtest:dbtest@localhost/dbtest";

    #[model]
    pub struct Test {
        pub field1: i64,
    }

    impl Test {
        pub fn new() -> Self {
            Self::default()
        }
    }

    impl Default for Test {
        fn default() -> Self {
            Self { id: -1, field1: 0 }
        }
    }

    #[test]
    fn t_1_reset() {
        use std::process::Command;

        Command::new("diesel")
            .args(&["migration", "redo", "--database-url", DATABASE_URL.clone()])
            .output()
            .expect("failed to execute process");
    }

    #[test]
    fn t_2_save_create() {
        let db = PgConnection::establish(&DATABASE_URL).unwrap();

        let mut test = Test::new();

        test.save(&db).unwrap();

        assert_eq!(test.id, 1);
    }

    #[test]
    fn t_3_fetch() {
        let db = PgConnection::establish(&DATABASE_URL).unwrap();

        let test = Test::fetch(1, &db).unwrap();
        // let test2 = Test::fetch_by_field1(0, &db).unwrap();

        assert_eq!(test.id, 1);
        assert_eq!(test.field1, 0);
        // assert_eq!(test2.id, 1);
        // assert_eq!(test2.field1, 0);
    }
}
